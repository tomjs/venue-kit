# Venue Kit

A collection of scripts for managing performance venues

## Curtain

curtain-control.lsl - A curtain controller that supports both sliding
curtains and fade-in/fade-out operation.

## HUD

venue_hud.lsl - A HUD that includes camera positions for specific
venues, curtain control and tipjar login/logout buttons.

### Versions

Multiple versions of the venue_hud script may be built with various modules
enabled/disabled to save memory.  GPP must be installed.

## Tipjar

tipjar.lsl - A tipjar script for performers to log in and out of with remote
control abilities.

## Build

A build step has been added to these scripts it simplify removing unneeded parts of the scripts for various configurations of HUS, curtains and tipjar.

The build step is actually just the GPP preprocessor in a C-like mode that is altered a bit to accomodate LSL.

* Pre-processor comments are introduced with //# and continue until the end of the line

		+c "//#" "\\n" \

* User-mode macros

		-U "" "" "(" "," ")" "(" ")" "#" "\\" \

	* macrons do not start or end with any special characters
	* macro arguments begin and end with parens and are separated by commas
	* parens are balanced
	* a hash refers to an argument by number
	* a backslash is used to quote characters

* User-mode meta-macros

		-M "\\n#\\w" "\\n" "\\b" "\\b" "\\n" "(" ")" \

	* a meta-macro begins with a newline immediately followed by a hash and the macro name
	* arguments are space-separated and end with a newline
	* parens are balanced

Build targets are present for the common build combinations.  The built scripts will appear in the ``build`` directory.

* ``vh-all`` - the whole enchilada
* ``vh-cam`` - a camera-only 4 button HUD
* ``vh-qob`` - The QoB 8 button version HUD
