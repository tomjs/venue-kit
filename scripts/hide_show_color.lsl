// hide_show_color - Control an object's visible attributes
// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2018 Layne Thomas
//
// Supported functions:
// * hide/show
// * flash glow
#ifdef COLOR_ENABLED
// * color
// * alpha
// * glow
#endif
//
// v2
// v3 - Add glow, fix step ranges
// v4 - Add color, alpha
// v5 - Fix link in color/alpha calls
// v6 - Add flash glow and glow command
// v7 - Add memory constraints
//
// Commands:
// reset                Reset to starting condition
// hide [<seconds>]     Hide the object in <seconds> (defaults to FADE_TIME)
// show [<seconds>]     Show the object in <seconds> (defaults to FADE_TIME)
#ifdef COLOR_ENABLED
// color <color> [<side>]       Set the object color
// alpha <alpha>                Set the object alpha
// coloralpha <color> <alpha>   Set both color and alpha at the same time
// glow <glow>                  Set the object glow
#endif
//
// Example:
// /10201 show 4.5      Show the object in 4.5 seconds
// /10201 hide          Hide the object in FADE_TIME seconds
#ifdef COLOR_ENABLED
// /10201 color <1,1,1>
#endif
//
// Settings below
#ifndef DEFAULT_COMM_CHANNEL
#define DEFAULT_COMM_CHANNEL 66
#endif
// * Set the channel (CHANNEL) (default DEFAULT_COMM_CHANNEL)
// * Set the starting viaibility (START_VISIBLE)
// * Set the default time for transition in seconds (0.0 for instant) (FADE_TIME)
// * Set the link value to operate on only the link the object is in or the entire linkset (link)

// Listen channel
integer CHANNEL = DEFAULT_COMM_CHANNEL;

// Rez object in this state
integer START_VISIBLE = FALSE;

// Set the limits of transparency
float MIN_ALPHA = 0.0;
float MAX_ALPHA = 1.0;
float alpha_step;

// Set the glow
float MIN_GLOW = 0.0;
float MAX_GLOW = 0.0;
float glow_step;

// Set the starting glow value when showing the object
// Setting this high-ish causes the object to 'flash' in
// as it becomes visible.  Set MAX_GLOW low (0.0) for full effect
float FLASH_GLOW = 0.05;

// Time to make the transition
float FADE_TIME = 2.0;

// Number of steps for the transition
integer FADE_STEPS = 10;

// Target
// Set this to only affect the link that this script is in
//integer link = LINK_THIS;

// Set this to affect the entire link set
integer link = LINK_SET;

// Be noisy!
integer VERBOSE = FALSE;

// Memory
integer MEM_LIMIT = 16000;

// Visibility state;
integer visible;

// Listen handles
integer hChannel = 0;

#ifdef DEBUG
debug(string msg) {
    if (VERBOSE) {
        llOwnerSay(msg);
    }
}
#endif

hide(float seconds) {
#ifdef DEBUG
    debug("hide("+(string)seconds+") visible="+(string)visible);
#endif
    llSetText("", <1,1,1>, 1);
    if (seconds > 0 && visible) {
        float display_alpha = MAX_ALPHA;
        float display_glow = FLASH_GLOW;
        // fade here
        float interval = seconds / (float)FADE_STEPS;
        integer i;
        for ( ; i < FADE_STEPS-1; ++i ) {
            llSleep(interval);
            display_alpha -= alpha_step;
            display_glow -= glow_step;
            llSetLinkAlpha(link, display_alpha, ALL_SIDES);
            llSetLinkPrimitiveParamsFast(LINK_SET, [
                PRIM_GLOW, ALL_SIDES, display_glow
            ]);
        }
    }
    llSetLinkAlpha(link, MIN_ALPHA, ALL_SIDES);
    llSetLinkPrimitiveParamsFast(LINK_SET, [
        PRIM_GLOW, ALL_SIDES, MIN_GLOW
    ]);
    visible = FALSE;
}

show(float seconds) {
#ifdef DEBUG
    debug("show("+(string)seconds+") visible="+(string)visible);
#endif
    llSetText("", <1,1,1>, 1);
    if (seconds > 0 && !visible) {
        float display_alpha = MIN_ALPHA;
        float display_glow = FLASH_GLOW;
        // show here
        float interval = seconds / (float)FADE_STEPS;
        integer i;
        for ( ; i < FADE_STEPS-1; ++i ) {
            llSleep(interval);
            display_alpha += alpha_step;
            display_glow += glow_step;
            llSetLinkAlpha(link, display_alpha, ALL_SIDES);
            llSetLinkPrimitiveParamsFast(LINK_SET, [
                PRIM_GLOW, ALL_SIDES, display_glow
            ]);
        }
    }
    llSetLinkAlpha(link, MAX_ALPHA, ALL_SIDES);
    llSetLinkPrimitiveParamsFast(LINK_SET, [
        PRIM_GLOW, ALL_SIDES, MAX_GLOW
    ]);
    visible = TRUE;
}

toggle(float seconds) {
    if (visible == TRUE) {
        hide(seconds);
    } else {
        show(seconds);
    }
}

#ifdef COLOR_ENABLED
// if end_color is TOUCH_INVALID_TEXCOORD color remains unchged
slide_color(integer link, integer face, vector end_color, float end_alpha) {
    vector color_step = ZERO_VECTOR;
    float alpha_step = 0.0;
    integer steps = 25;
    float wait = 2.0 / steps;
    if (wait < 0.02) wait = 0.02;
    if (face == ALL_SIDES) face = 0;
    list d = llGetLinkPrimitiveParams(
        link, [
            PRIM_COLOR, face
        ]
    );
    vector start_color = llList2Vector(d, 0);
    float start_alpha = llList2Float(d, 1);
#ifdef DEBUG
    debug("face="+(string)face+"  d="+llList2CSV(d));
    debug("start_color="+(string)start_color+"  start_alpha="+(string)start_alpha);
#endif

    // Use TOUCH_INVALID_TEXCOORD as a skip marker
    if (end_color == TOUCH_INVALID_TEXCOORD) {
        end_color = start_color;
    } else {
        color_step = <
            (end_color.x - start_color.x) / steps,
            (end_color.y - start_color.y) / steps,
            (end_color.z - start_color.z) / steps
        >;
    }
    // Use negative alpha as a skip value
    if (end_alpha < 0) {
        end_alpha = start_alpha;
    } else {
        alpha_step = (end_alpha - start_alpha) / steps;
    }

#ifdef DEBUG
    debug("color_step="+(string)color_step+"  alpha_step="+(string)alpha_step);
#endif
    integer i;
    for(i=1; i<=steps; i++) {
        start_color += color_step;
        llSetLinkColor(link, start_color, face);
        start_alpha += alpha_step;
        llSetLinkAlpha(link, start_alpha, face);
        llSleep(wait);
    }
    llSetLinkColor(link, end_color, face);
    llSetLinkAlpha(link, end_alpha, face);
}
#endif

reset() {
    alpha_step = ((MAX_ALPHA - MIN_ALPHA)/(float)FADE_STEPS);
    glow_step = ((MAX_GLOW - MIN_GLOW)/(float)FADE_STEPS);

    visible = START_VISIBLE;
    if (visible) {
        show(0);
    } else {
        hide(0);
    }

    llListenRemove(hChannel);

    if (CHANNEL != 0) {
        hChannel = llListen(CHANNEL, "", NULL_KEY, "");
    }
#ifdef DEBUG
    debug("Listening on channel " + (string)CHANNEL);
#endif
}

default {
    state_entry() {
        llSetMemoryLimit(MEM_LIMIT);
        reset();
//        llOwnerSay("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
    }

    listen(integer channel, string name, key id, string message) {
        // Parse command
        list _args = llParseString2List(message, [ " " ], []);

        string cmd = llToLower(llList2String(_args, 0));
#ifdef DEBUG
        debug("cmd: " + cmd);
#endif

        // Also parse the first arg as a float (for seconds, alpha, glow values)
        float floatval = -1.0;
        if (llGetListLength(_args) > 1) {
            floatval = llList2Float(_args, 1);
        } else {
            floatval = FADE_TIME;
        }

        if (cmd == "reset") {
            reset();
        } else if (cmd == "hide") {
            hide(floatval);
        } else if (cmd == "show") {
            show(floatval);
        } else if (cmd == "toggle") {
            toggle(floatval);
#ifdef COLOR_ENABLED
        } else if (cmd == "color") {
            // Get a color from arg1
            vector color = (vector)llList2String(_args, 1);
            slide_color(link, 1, color, -1.0);
        } else if (cmd == "alpha") {
            // Get an alpha from arg 1
            slide_color(link, 1, TOUCH_INVALID_TEXCOORD, floatval);
        } else if (cmd == "coloralpha") {
            // Get a color from arg 1
            vector color = (vector)llList2String(_args, 1);
            float alpha = llList2Float(_args, 2);
            slide_color(link, 1, color, alpha);
        } else if (cmd == "glow") {
            // Get a glow from arg 1
            llSetLinkPrimitiveParamsFast(LINK_SET, [
                PRIM_GLOW, ALL_SIDES, floatval
            ]);
#endif
        }
    }
}
