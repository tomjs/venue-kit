// load_notecard.lsl - Load INI-format notecards
// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Layne Thomas
//
// Load notecards from inventory
//
// v0 - new
// v1 - Use LSD storage
// v2 - Now with fewer hard-coded keys!
//
// Link Messages recognized:
// 42001 - set VERBOSE
// 42002 - set notecard prefix (default: CONFIG)
// 42011 - show load notecard menu for selection
// 42012 - load specified notecard (without notecard prefix)
//
// Link Messages returned:
// 200 - OK
// 3xx - Error
// 301 - notecard reading in progress
// 302 - notecard not found

#ifdef VENUE_ENABLED
// * load_venue
#endif

string NOTECARD_PREFIX = "CONFIG";
string DEFAULT_SECTION_NAME = "default";
list LOAD_OPTIONS = ["ALL", "CANCEL", "."];
string LOAD_TEXT = "Select a notecard to load";
integer LISTEN_CHANNEL = -1;
float MENU_TIMEOUT = 30.0;
integer listen_handle;
#ifdef VENUE_ENABLED
string LSD_PREFIX = "venue_";
#else
string LSD_PREFIX = "key_";
#endif

// Internal vars
string notecard_name;
key notecard_qid;
integer reading_notecard;
integer line;
string current_section;
string section_json;

// Config notecards in inventory
list inv_cards;
integer card_num;

integer VERBOSE = FALSE;
integer MEM_LIMIT = 24000;

log(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

// See if the notecard is present in object inventory
integer can_haz_notecard(string name) {
    integer count = llGetInventoryNumber(INVENTORY_NOTECARD);
    while (count--) {
        if (llGetInventoryName(INVENTORY_NOTECARD, count) == name) {
            return TRUE;
        }
    }
    return FALSE;
}

// Find all notecards in inventory beginning with notecard prefix
// Appends '_' to the configured NOTECARD_PREFIX
get_notecards() {
    integer prefix_len = llStringLength(NOTECARD_PREFIX) - 1;
    inv_cards = [];
    string name;
    integer count = llGetInventoryNumber(INVENTORY_NOTECARD);
    while (count--) {
        name = llGetInventoryName(INVENTORY_NOTECARD, count);
        if (name == NOTECARD_PREFIX) {
            // Notecard name is just the prefix
            inv_cards += [name];
        }
        else if (llGetSubString(name, 0, prefix_len + 1) == NOTECARD_PREFIX + "_") {
            // Notecard name is PREFIX_xxxx
            inv_cards += [llGetSubString(name, prefix_len + 2, -1)];
        }
    }
}

// Load the specified notecard
integer load_notecard(string name) {
    notecard_name = name;
    if (name != NOTECARD_PREFIX) {
        notecard_name = NOTECARD_PREFIX + "_" + name;
    }

    if (can_haz_notecard(notecard_name)) {
        llOwnerSay("Reading notecard: " + notecard_name);
        line = 0;
        // Storage
        current_section = DEFAULT_SECTION_NAME;
        section_json = "";
        reading_notecard = TRUE;
        llSetTimerEvent(0.4);
        return TRUE;
    } else {
        llOwnerSay("Notecard " + notecard_name + " not found");
        reading_notecard = FALSE;
        return FALSE;
    }
}

save_section() {
    // Save what we have
    llLinksetDataWrite(LSD_PREFIX + current_section, section_json);

#ifdef VENUE_ENABLED
    // Add to 'venues' key
    list venue_list;
    string lsd_venues = llLinksetDataRead("venues");
    lsd_venues = llJsonSetValue(lsd_venues, [current_section, "center"], llJsonGetValue(section_json, ["center"]));
    lsd_venues = llJsonSetValue(lsd_venues, [current_section, "distance"], llJsonGetValue(section_json, ["distance"]));
    lsd_venues = llJsonSetValue(lsd_venues, [current_section, "region"], llJsonGetValue(section_json, ["region"]));
    llLinksetDataWrite("venues", lsd_venues);
#endif

    // Clean up for next line
    current_section = "";
    section_json = "";
}

read_config(string data) {
    if (data == EOF) {
        // All done
        save_section();
        return;
    }

    data = llStringTrim(data, STRING_TRIM_HEAD);
    if (data != "" && llSubStringIndex(data, "#") != 0) {
        if (llSubStringIndex(data, "[") == 0) {
            if (current_section != "") {
                // Save previous section if valid
                save_section();
            }
            // Section header
            integer end = llSubStringIndex(data, "]");
            if (end != 0) {
                // Well-formed section header
                current_section = llGetSubString(data, 1, end-1);
                section_json = llJsonSetValue(section_json, ["name"], current_section);
#ifdef VENUE_ENABLED
                llOwnerSay("Loading venue: " + current_section);
#else
                llOwnerSay("Loading section: " + current_section);
#endif
                // Reset section globals
            }
        } else {
            integer i = llSubStringIndex(data, "=");
            if (i != -1) {
                string attr = llToLower(llStringTrim(llGetSubString(data, 0, i-1), STRING_TRIM));
                string value = llStringTrim(llGetSubString(data, i+1, -1), STRING_TRIM);
                list keys = llParseStringKeepNulls(attr, [".", "_"], []);
                if (llStringLength(data) == i+1) {
                    // no value so force it empty
                    value = "";
                }

                // Leave one hard-coded key to maintain a good structure
                if (attr == "last_seen") {
                    section_json = llJsonSetValue(section_json, [attr], value);
                }
#ifdef VENUE_ENABLED
                else if (llList2String(keys, 0) == "cam") {
                    // Leave this for back-compat for now
                    section_json = llJsonSetValue(section_json, [attr], value);
                    // Save nested version of cams
                    section_json = llJsonSetValue(section_json, keys, value);
                }
#endif
                else {
                    // Save all other attributes after parsing on '.' and/or '_'
                    section_json = llJsonSetValue(section_json, keys, value);
                }
            } else {
                if (llSubStringIndex(data, "//") >= 0) {
                    // Skip old '//' comments
                } else {
                    // not an assignment line
                    llOwnerSay("Configuration could not be read on line " + (string)line);
                }
            }
        }
    }
 
    notecard_qid = llGetNotecardLine(notecard_name, ++line);
}

init() {
//    log("load: Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());

    // Generate a very negative menu channel
    LISTEN_CHANNEL = ((integer)("0x"+llGetSubString((string)llGetKey(),-8,-1)) & 0x3FFFFFFF) ^ 0xBFFFFFFF;

    get_notecards();
    // Mark not reading all cards
    card_num = -1;
    reading_notecard = FALSE;
}

default {
    on_rez(integer start_param) {
        init();
    }
 
    changed(integer change) {
        if(change & (CHANGED_OWNER | CHANGED_INVENTORY))
            init();
    }
 
    state_entry() {
        llSetMemoryLimit(MEM_LIMIT);
//        llOwnerSay("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
        init();
    }
 
    dataserver(key request_id, string data) {
        if (request_id == notecard_qid) {
            read_config(data);
            if (data == EOF) {
                // Do whatever happens after the notecard is read
                if (card_num >= 0) {
                    // We're loading all cards, call the next one
                    card_num++;
                    if (card_num < llGetListLength(inv_cards)) {
                        load_notecard(llList2String(inv_cards, card_num));
                    } else {
                        card_num = -1;
                           reading_notecard = FALSE;
                        llMessageLinked(LINK_SET, 200, "ALL", NULL_KEY);
                        llOwnerSay("Loading complete");
                    }
                } else {
                    reading_notecard = FALSE;
                    llMessageLinked(LINK_SET, 200, notecard_name, NULL_KEY);
                    llOwnerSay("Loading complete");
                }
            }
        }
    }

    listen(integer channel, string name, key id, string message) {
        if (channel == LISTEN_CHANNEL) {
            llSetTimerEvent(0);
            llListenRemove(listen_handle);
            if (message == "ALL") {
                log("ALL");
                // Start loading them all with the first one
                card_num = 0;
                load_notecard(llList2String(inv_cards, card_num));
            }
            else if (message == "CANCEL") {
                log("message: " + message);
                // Clean up anything left
            }
            else {
                log("message: " + message);
                card_num = -1;
                load_notecard(message);
            }
        }
    }

    link_message(integer sender, integer num, string str, key id) {
        if (num == 42001) {
            // Set VERBOSE
            VERBOSE = num;
        }
        else if (num == 42002) {
            // Set prefix
            NOTECARD_PREFIX = str;
            get_notecards();
        }
        else if (num == 42011) {
            // Load notecard selected from list
            llListenRemove(listen_handle);
            listen_handle = llListen(LISTEN_CHANNEL, "", NULL_KEY, "");
            llDialog(llGetOwner(), LOAD_TEXT, LOAD_OPTIONS + inv_cards, LISTEN_CHANNEL);
            llSetTimerEvent(MENU_TIMEOUT);
        }
        else if (num == 42012) {
            // Load specific notecard
            if (reading_notecard) {
                // Send back a fail
                llMessageLinked(LINK_SET, 301, "ERROR: read error", NULL_KEY);
            } else {
                // Read the requested card
                if (!load_notecard(str)) {
                    llMessageLinked(LINK_SET, 302, "ERROR: not found", NULL_KEY);
                }
            }
        }
    }

    timer() {
        llSetTimerEvent(0);
        if (reading_notecard) {
            notecard_qid = llGetNotecardLine(notecard_name, line);
        } else {
            llListenRemove(listen_handle);
        }
    }
}
