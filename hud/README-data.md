# HUD Data Structures

The HUD uses LSD to store the venue configurations

## Keys

`venues` - object
	"<name>" - string - Display name for venue
		`center` - vector (string) - Reference position
		`distance` - vector (string) - Distance from reference position to be considered 'at' the venue (default: <100, 100, 100>)
		`region` - string - Region name

`venue_*` - object
	`camN` - object
		`pos` - vector (string) - camera position
		`rot` - vector (string) - camera rotation (Euler coords)
	`center` - vector (string) - Reference position
	`comm` - object
		`channel` - integer - General communications channel
	`curtain` object
		`channel` - integer - Curtain comm channel
		`open` - string - Open command
		`close` - string - Close command
	`distance` - vector (string) - Distance from reference position to be considered 'at' the venue (default: <100, 100, 100>)
	`region` - string - Region name
	`tipjar` - object
		`channel` - integer - Curtain comm channel
		`login` - string - Login command
		`logout` - string - Logout command
	`stage` - object
		`channel` - integer - Curtain comm channel
		`hide` - string - Hide command
		`show` - string - Show command
	`version` - string - An version identifier for the notecard

# Examples

## Noir Neverland

```json
"venues": {
	"Noir Neverland": {
		"center": "<54.80000, 37.92000, 3185.73462>",
		"distance":"<20, 20, 10>",
		"region": "Tropic Breeze"
	},
	"Queens of Burlesque": {
		"center": "<201.00000, 191.00000, 3003.00000>",
		"region": "Beachwood"
	}
}

"venue_Noir Neverland": {
	"cam_1": "<103.00000, 62.00000, 3015.00000>, <0.00000, 0.00000, 180.00000>",
	"cam_2": "<85.00000, 62.00000, 3020.00000>, <0.00000, -44.00000, 180.00000>",
	"cam_3": "<57.50000, 62.00000, 3017.00000>, <0.00000, 42.00000, 0.00000>",
	"cam_4": "<101.00000, 67.50000, 3009.00000>, <0.00000, 0.00000, 180.00000>",
	"center":"<54.80000, 37.92000, 3185.73462>",
	"comm": {
		"channel": 7
	},
	"curtain": {
		"channel": 42,
		"close": "close",
		"open": "open"
	},
	"distance":"<20, 20, 10>",
	"region":"Tropic Breeze",
	"tipjar": {
		"channel": 70,
		"login": "login",
		"logout": "logout"
	},
	"version": "2022-12-25"
}
```

### Notecard

```
[Noir Neverland]
version=2022-12-25
# venues
region=Tropic Breeze
center=<54.80000, 37.92000, 3185.73462>
distance=<100, 100, 100>
# venue-<name>
comm_channel=7
curtain_channel=42
curtain_close=close
curtain_open=open
tipjar_channel=70
tipjar_login=login
tipjar_logout=logout
cam_1=<103.00000, 62.00000, 3015.00000>, <0.00000, 0.00000, 180.00000>
cam_2=<85.00000, 62.00000, 3020.00000>, <0.00000, -44.00000, 180.00000>
cam_3=<57.50000, 62.00000, 3017.00000>, <0.00000, 42.00000, 0.00000>
cam_4=<101.00000, 67.50000, 3009.00000>, <0.00000, 0.00000, 180.00000>
```
