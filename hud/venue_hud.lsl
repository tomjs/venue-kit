// venue_hud - Common control for venue HUDs
// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2020-2024 Layne Thomas
//
#ifdef VENUE
// Venue: VENUE
//
#endif
// This script contains the basic structure and specific sections for HUD
// funtionality useful for performance venues:
#ifdef CAMERA_ENABLED
// * useful common camera positions
#endif
#ifdef COMM_ENABLED
// * semi-private communication between wearers
#endif
#ifdef CURTAIN_ENABLED
// * curtain open/close
#endif
#ifdef RLV_ENABLED
// * RLV-based lock to prevent HUD from being removed during costume changes
#endif
#ifdef TIPJAR_ENABLED
// * tipjar login/logout
#endif
//
// v1 - Forked from hud_control v10 for venue-specific HUDs
// v2 - Fix camera resets on TP out
// v3 - Rework for G3 HUD, use GPP to include/remove certain functionality
// v4 - Reconcile ACT script, camera updates for Lower Stage, minor fixes
// v5 - Add LSD support, Settings menu, Notecard load
// v6 - Add List Cards
// v7 - Fix attach venue detection and curtain commands, experimental Discord support
// v8 - Support complex chat commands; fix camera position output to match notecard format
//      set comm channed in venue notecard
// v9 - Rework QoB-specific bits to 4b and stage hide/show
//
// Description field:
#ifdef COMM_ENABLED
#ifndef DEFAULT_COMM_CHANNEL
#define DEFAULT_COMM_CHANNEL 6
#endif
// channel - set the COMM channel (default DEFAULT_COMM_CHANNEL)
#endif
#ifdef TIPJAR_ENABLED
#ifndef DEFAULT_TIPJAR_CHANNEL
#define DEFAULT_TIPJAR_CHANNEL 71
#endif
// tipjar - tipjar remote channel (default DEFAULT_TIPJAR_CHANNEL)
#endif
#ifeq VENUE qob
#ifndef DEFAULT_STAGE_CHANNEL
#define DEFAULT_STAGE_CHANNEL 2024
#endif
#endif
// log - enable VERBOSE logging

#define NUM_ROWS 1
#define NUM_BUTTONS 4

// Enabled modules:
#ifdef ACT_ENABLED
// * ACT
#endif
#ifeq VENUE qob
// * Queens of Burlesque
#define QOB_FLOOR_CURTAIN_CHANNEL 4242
#define NUM_BUTTONS 8
#endif

#ifdef CAMERA_ENABLED
// CAMERA
#endif
#ifdef COMM_ENABLED
// COMM
#endif
#ifdef CURTAIN_ENABLED
// CURTAIN
#endif
#ifdef DISCORD_ENABLED
// DISCORD
#endif
#ifdef RLV_ENABLED
// RLV
#endif
#ifdef TIPJAR_ENABLED
// TIPJAR
#endif

#ifdef COMM_ENABLED
// The channel for inter-HUD communication
integer COMM_CHANNEL = DEFAULT_COMM_CHANNEL;
#endif
#ifdef DISCORD_ENABLED
#ifndef DEFAULT_WEBHOOK_CHANNEL
#define DEFAULT_WEBHOOK_CHANNEL "add-channel-here"
#endif
string WEBHOOK_CHANNEL = DEFAULT_WEBHOOK_CHANNEL;
#ifndef DEFAULT_WEBHOOK_TOKEN
#define DEFAULT_WEBHOOK_TOKEN "add-token-here"
#endif
string WEBHOOK_TOKEN = DEFAULT_WEBHOOK_TOKEN;
#ifndef DEFAULT_WEBHOOK_URL
#define DEFAULT_WEBHOOK_URL "https://discordapp.com/api/webhooks/"
#endif
string WEBHOOK_URL = DEFAULT_WEBHOOK_URL;

// Send message
integer DISCORD_SEND = 1500001;
#endif

// Control faces
integer minimize_face = 0;
integer rlv_face = 4;
integer cam_face = 6;
integer reset_face = 6;
integer settings_face = 2;

// Spew log info
integer VERBOSE = FALSE;

// Memory limit in bytes
#define DEFAULT_MEM_LIMIT 50000
integer MEM_LIMIT = DEFAULT_MEM_LIMIT;


// ****************************************
// Settings Dialog Stuff

list SETTINGS_OPTIONS = [
    "List Cards", "Reload HUD", "Clear Data", "Load Notecard"
#ifdef ACT_ENABLED
    ,"Set Title"
#endif
];

string SETTINGS_TEXT = "HUD Settings";
integer LISTEN_CHANNEL = -1;
integer MENU_CHANNEL;
integer menu_handle;
float MENU_TIMEOUT = 30.0;
#define LOAD_SCRIPT_NAME "~load venue"
string load_script_name = LOAD_SCRIPT_NAME;

// ****************************************
// Venue Stuff

// Venue globals
string venue_version;
string venue_name;
vector venue_center;
vector venue_distance;
string region_name;

#ifdef CURTAIN_ENABLED
// ****************************************
// Curtain stuff

integer curtain_channel;
list curtain_close;
list curtain_open;
#endif

#ifdef TIPJAR_ENABLED
// ****************************************
// Tipjar stuff

integer tipjar_channel = DEFAULT_TIPJAR_CHANNEL;
list tipjar_login;
list tipjar_logout;
#endif

#ifeq VENUE qob
// ****************************************
// QoB Stage stuff

integer stage_channel = DEFAULT_STAGE_CHANNEL;
list stage_hide;
list stage_show;
#endif

#ifdef CAMERA_ENABLED
// ****************************************
// Camera stuff

// stride 2 list <pos>, <rot-in-deg>
list cam_slots = [];

vector cam_pos;
vector cam_rot;
integer cam_index;
integer cam_is_set = FALSE;
float cam_alpha = 1.00;
vector cam_color_active = <0.00, 0.80, 0.00>;
vector cam_color_inactive = <0.93, 1.00, 1.00>;
integer cam_link;
integer camsel_link;
integer camsel_face;
vector camsel_rot_in = <0.00, 0.00, 0.00>;
vector camsel_rot_out = <0.00, 90.00, 0.00>;
integer camsel_visible = FALSE;

// Show cam select button
do_showcam() {
    rotation localRot = llList2Rot(llGetLinkPrimitiveParams(camsel_link, [PRIM_ROT_LOCAL]), 0);
    if (camsel_visible) {
        llSetLinkPrimitiveParamsFast(cam_link, [
            PRIM_COLOR, cam_face, cam_color_active, cam_alpha
        ]);
        llSetLinkPrimitiveParamsFast(camsel_link, [
            PRIM_ROT_LOCAL, llEuler2Rot(camsel_rot_in * DEG_TO_RAD)
        ]);
    } else {
        llSetLinkPrimitiveParamsFast(cam_link, [
            PRIM_COLOR, cam_face, cam_color_inactive, cam_alpha
        ]);
        llSetLinkPrimitiveParamsFast(camsel_link, [
            PRIM_ROT_LOCAL, llEuler2Rot(camsel_rot_out * DEG_TO_RAD)
        ]);
    }
}

// Camera LSD Interface
//
// Key: cam.
// Read camera position from LSD
get_cam_pos() {
}

// Write camera position to LSD
put_cam_pos(vector pos, vector rot, integer index) {
    // set cam pos
    cam_slots = llListReplaceList(
        cam_slots,
        [pos, rot],
        index,
        index+1
    );

    // Write to LSD
}
#endif

#ifdef DISCORD_ENABLED
// ****************************************
// Discord stuff

string DISCORD_NAME = "discord";
integer haz_discord = FALSE;

integer can_haz_discord() {
    // See if the Discord script is present in object inventory
    integer count = llGetInventoryNumber(INVENTORY_SCRIPT);
    while (count--) {
        if (llGetInventoryName(INVENTORY_SCRIPT, count) == DISCORD_NAME) {
            log("Discord enabled");
            return TRUE;
        }
    }
    return FALSE;
}

integer WEBHOOK_WAIT = TRUE;

// From https://github.com/TBGRenfold/discord-lsl/blob/master/discord_example.lsl
string slurl(key AvatarID) {
    string regionname = llGetRegionName();
    vector pos = llList2Vector(llGetObjectDetails(AvatarID, [ OBJECT_POS ]), 0);

    return "http://maps.secondlife.com/secondlife/"
        + llEscapeURL(regionname) + "/"
        + (string)llRound(pos.x) + "/"
        + (string)llRound(pos.y) + "/"
        + (string)llRound(pos.z) + "/";
}

key PostToDiscordX(key AvatarID, string Message) {
    list json = [
//        "username", llGetObjectName() + "",
        "username", llGetUsername(AvatarID) + "",
//        "content", llGetUsername(AvatarID) + ": " + Message + "\n\nProfile: http://my.secondlife.com/" + llGetUsername(AvatarID) + "\nLocation: " + slurl(AvatarID),
        "content", Message,
//        "avatar_url", "https://my-secondlife-agni.akamaized.net/users/" + llGetUsername(AvatarID) + "/sl_image.png",
         "tts", "false"
    ];
    string query_string = "";
    if (WEBHOOK_WAIT)
        query_string += "?wait=true";

    return llHTTPRequest(WEBHOOK_URL + WEBHOOK_CHANNEL + "/" + WEBHOOK_TOKEN + query_string,
    [
        HTTP_METHOD, "POST",
        HTTP_MIMETYPE, "application/json",
        HTTP_VERIFY_CERT,TRUE,
        HTTP_VERBOSE_THROTTLE, TRUE,
        HTTP_PRAGMA_NO_CACHE, TRUE ], llList2Json(JSON_OBJECT, json));
}
#endif

#ifdef COMM_ENABLED
// ****************************************
// Comm stuff

// Shown in front of all text from another HUD, along with the channel number
// Set to empty string to turn off
string COMM_PREFIX_BASE = "> ";
string comm_prefix;
integer comm_listen;
integer repeat_listen;

tune_channel() {
    if (comm_listen)
        llListenRemove(comm_listen);
    if (repeat_listen)
        llListenRemove(repeat_listen);

    if (!COMM_CHANNEL) {
        comm_listen = 0;
        repeat_listen = 0;
        llSetLinkPrimitiveParamsFast(LINK_ROOT, [
            PRIM_TEXT, make_label(), <1,1,1>, 1
        ]);
        return;
    }

    comm_listen = llListen(COMM_CHANNEL, "", owner, "");
    repeat_listen = llListen(-COMM_CHANNEL, "", NULL_KEY, "");
    llOwnerSay("Listening on channel " + (string)COMM_CHANNEL);
    llSetLinkPrimitiveParamsFast(LINK_ROOT, [
        PRIM_TEXT, make_label(), <1,1,1>, 1
    ]);
    if (COMM_PREFIX_BASE != "")
        comm_prefix = (string)COMM_CHANNEL + COMM_PREFIX_BASE;
}
#endif

// ****************************************
// HUD Positioing

// HUD Positioning offsets

#ifdef ACT_ENABLED
// ACT HUD
float top_offset = -0.08;
float bot_offset = 0.075;

float left_offset = -0.04;
float right_offset = 0.21;
float center_offset = 0.00;
#else
#ifeq NUM_ROWS 1
// Standard 1 row HUD
float top_offset = -0.05;
float bot_offset = 0.075;
#else
// Standard 2 row HUD
float top_offset = -0.07;
float bot_offset = 0.095;
#endif

#ifeq NUM_BUTTONS 4
// Standard 4 wide HUD
float left_offset = -0.14;
float right_offset = 0.18;
float center_offset = 0.00;
#else
// Standard 8 wide HUD
float left_offset = -0.36;
float right_offset = 0.18;
float center_offset = -0.20;
#endif
#endif
//# // Standard 6 wide HUD
//# float left_offset = -0.24;
//# float right_offset = 0.18;

integer last_attach = 0;
integer attach_bottom = FALSE;

do_hide(integer minimize) {
    log("hide("+(string)minimize+") attach_bottom: "+(string)attach_bottom);
    if (minimize) {
        float rot = PI_BY_TWO;
//#        if (!attach_bottom) {
            rot *= -1;
//#        }
        rotation localRot = llList2Rot(llGetLinkPrimitiveParams(LINK_ROOT, [PRIM_ROT_LOCAL]), 0);
        llSetLinkPrimitiveParamsFast(LINK_ROOT, [PRIM_ROT_LOCAL, llEuler2Rot(<0.0, 0.0, rot>)*localRot]);
    } else {
        llSetLinkPrimitiveParamsFast(LINK_ROOT, [PRIM_ROT_LOCAL, ZERO_ROTATION]);
    }
    set_text(!minimize);
}
// ****************************************

key owner;

button(integer link, integer face, vector p, integer long) {
    string name = llGetLinkName(link);
    log("button() name: " + name + "  face: " + (string)face + "  pos: " + (string)p);

    if (name == "back") {
        if (long) {
            // RESET button
            llResetScript();
        }
    }
    else if (name == "c0" || name == "c1") {
// ****************************************
// Settings Dialog Stuff
        if (face == settings_face) {
            // Settings
            llListenRemove(menu_handle);
            menu_handle = llListen(MENU_CHANNEL, "", NULL_KEY, "");
            llDialog(llDetectedKey(0), SETTINGS_TEXT, SETTINGS_OPTIONS, MENU_CHANNEL);
            llSetTimerEvent(MENU_TIMEOUT);
        }
        else if (face == minimize_face) {
            if (long) {
                // Detatch
                llRequestPermissions(owner, PERMISSION_ATTACH);
            } else {
                // Minimize
                do_hide(name == "c0");
            }
        }
#ifdef CAMERA_ENABLED
// ****************************************
// Camera stuff
        else if (face == cam_face) {
            if (long) {
                // show cam position
                llRequestPermissions(owner, PERMISSION_TRACK_CAMERA);
            } else {
                camsel_visible = !camsel_visible;
                do_showcam();
                if (!camsel_visible && llGetAttached()) {
                    // off
                    cam_is_set = FALSE;
                    llRequestPermissions(owner, PERMISSION_CONTROL_CAMERA);
                }
            }
        }
#endif
#ifdef RLV_ENABLED
// ****************************************
// RLV Stuff
        else if (face == rlv_face) {
            // RLV Lock
            if (rlv_locked) {
                unlockme();
            } else {
                lockme();
            }
        }
#endif
// ****************************************
    }
    else if (name == "b1") {
#ifdef CURTAIN_ENABLED
// ****************************************
// Curtain stuff
        if (face == 0) {
            // Open curtain
            sendlist(curtain_open);
        }
        else if (face == 2) {
            // Close curtain
            sendlist(curtain_close);
        }
#endif
#ifdef TIPJAR_ENABLED
// ****************************************
// Tipjar Stuff
        else if (face == 4) {
            sendlist(tipjar_login);
        }
        else if (face == 6) {
            sendlist(tipjar_logout);
        }
#endif
#ifeq VENUE qob
// ****************************************
// QoB Stuff
        else if (face == 4) {
            sendlist(stage_hide);
        }
        else if (face == 6) {
            sendlist(stage_show);
        }
#endif
// ****************************************
    }
    else if (name == "b2") {
// ****************************************
    }
#ifdef CAMERA_ENABLED
// ****************************************
// Camera stuff
    else if (name == "camsel" && llGetAttached() && venue_name != "--") {
        if (long) {
            // show cam position
            llRequestPermissions(owner, PERMISSION_TRACK_CAMERA);
        } else {
            // handle 4 1x1 buttons
            integer bx = (integer)(face / 2);
            if (bx >= 0 && bx <= 3) {
                // Turn off prior selection
                llSetLinkPrimitiveParamsFast(camsel_link, [
                    PRIM_COLOR, camsel_face, cam_color_inactive, cam_alpha
                ]);
                log("cam_is_set: "+(string)cam_is_set+"  camsel_face: "+(string)camsel_face);
                if (cam_is_set && face == camsel_face) {
                    // Click it off
                    cam_is_set = FALSE;
                    llClearCameraParams(); // reset camera to default
                } else {
                    // (button * cam_stride)
                    cam_index = (bx * 2);
                    cam_pos = (vector)llList2String(cam_slots, cam_index);
                    cam_rot = (vector)llList2String(cam_slots, cam_index + 1);
                    camsel_face = face;
                    cam_is_set = TRUE;
                    log("pos="+(string)cam_pos+"  rot="+(string)cam_rot);
                }
                llRequestPermissions(owner, PERMISSION_CONTROL_CAMERA);
            }
        }
    }
#endif
// ****************************************
}

#ifdef RLV_ENABLED
// ****************************************
// RLV

float LOCKED_ALPHA = 1.0;
vector LOCKED_COLOR = <1.0, 0.0, 0.0>;
float UNLOCKED_ALPHA = 1.0;
vector UNLOCKED_COLOR = <0.0, 1.0, 0.0>;

integer rlv_channel = 12345;
integer rlv_handle;
integer rlv_version = 0;
integer rlv_locked = FALSE;
integer rlv_button;
string rlv_button_name = "c0";

init_rlv() {
    rlv_handle = llListen(rlv_channel, "", NULL_KEY, "");
    llSetTimerEvent(60.0);
    llOwnerSay("@versionnum="+(string)rlv_channel);
}

lockme() {
    if (rlv_version > 0) {
        llOwnerSay("HUD lock ON");
        llOwnerSay("@detach=n");
        rlv_locked = TRUE;
        llSetLinkPrimitiveParamsFast(rlv_button, [
            PRIM_COLOR, rlv_face, LOCKED_COLOR, LOCKED_ALPHA
        ]);
    }
}

unlockme() {
    if (rlv_version > 0) {
        llOwnerSay("HUD lock OFF");
        llOwnerSay("@detach=y");
        rlv_locked = FALSE;
        llSetLinkPrimitiveParamsFast(rlv_button, [
            PRIM_COLOR, rlv_face, UNLOCKED_COLOR, UNLOCKED_ALPHA
        ]);
    }
}
// ****************************************
#endif

// Based on SplitLine() from http://wiki.secondlife.com/wiki/SplitLine
string SplitLine(string _source, string _separator) {
    integer offset = 0;
    integer separatorLen = llStringLength(_separator);
    integer split = -1;

    do {
        split = llSubStringIndex(llGetSubString(_source, offset, -1), _separator);
        if (split != -1) {
            _source = llGetSubString(_source, 0, offset + split - 1) + "\\n" + llGetSubString(_source, offset + split + separatorLen, -1);
            offset += split + separatorLen;
        }
    } while (split != -1);
    return _source;
}

// Parses the object description into globals
// [channel=<comm-channel>][:tipjar=<tipjar-channel>][:log]
// All values are set directly and not returned

parse_desc(string desc) {
    list _args = llParseString2List(desc, [ ":" ], []);

    integer i = 0;
    string s = llList2String(_args, i);
    while (s != "") {
        // Look for key=value args
        list kv = llParseString2List(s, ["="], []);
        string _key = llList2String(kv, 0);
        string _val = llList2String(kv, 1);

        // Look for boolean args
        if (s == "log") {
            VERBOSE = TRUE;
        }

        // Look for a=v args
#ifdef COMM_ENABLED
        else if (_key == "channel") {
            COMM_CHANNEL = (integer)_val;
        }
#endif
#ifdef TIPJAR_ENABLED
        else if (_key == "tipjar") {
            tipjar_channel = (integer)_val;
        }
#endif

        i++;
        s = llList2String(_args, i);
    }
}

// ****************************************
// Venue Stuff

list parse_command(string str, integer default_channel) {
    list retlist;
    list parts = llParseString2List(str, ["|"], []);
    integer i = 0;
    string s = llList2String(parts, i);
    while (s != "") {
        // Look for <channel>:<command> pairs
        list cc = llParseString2List(s, [":"], []);
        integer chan;
        string cmd;
        if (llGetListLength(cc) < 2) {
            // No ':' found, assume it is the simple command string
            // Use default channel
            chan = default_channel;
            cmd = llList2String(cc, 0);
        } else {
            // We found <channel>:<command>
            chan = llList2Integer(cc, 0);
            cmd = llList2String(cc, 1);
        }

        retlist += [chan, cmd];

        i++;
        s = llList2String(parts, i);
    }
    return retlist;
}

list_venues() {
    // LSD
    string lsd_venues = llLinksetDataRead("venues");
    if (lsd_venues == "") {
        // Nothing found, exit
        return;
    }

    // Parse the stored venues into a list
    // List is stride 2: <name>, <json-object>
    list venue_list = llJson2List(lsd_venues);

    integer i = 0;

    // Look for a match
    while (i < llGetListLength(venue_list)) {
        string name = llList2String(venue_list, i);
        string region = llJsonGetValue(llList2String(venue_list, i+1), ["region"]);

        string venue = llLinksetDataRead("venue_" + name);
        venue_version = (string)llJsonGetValue(venue, ["version"]);

        llOwnerSay(name + "  " + venue_version);
        i += 2;
    }
}

// Sets globals: region_name, venue_center, venue_distance, venue_name, curtain_*, tipjar_*
integer find_venue() {
    // LSD
    string lsd_venues = llLinksetDataRead("venues");
    if (lsd_venues == "") {
        // Nothing found, exit
        return FALSE;
    }

    // Parse the stored venues into a list
    // List is stride 2: <name>, <json-object>
    list venue_list = llJson2List(lsd_venues);

    integer i = 0;
    venue_name = "--";
    venue_version = "";
    region_name = llGetRegionName();

    // Look for a match
    while (i < llGetListLength(venue_list)) {
        string name = llList2String(venue_list, i);
        string region = llJsonGetValue(llList2String(venue_list, i+1), ["region"]);
        if (region == region_name) {
            // Found a region match, check for position
            vector center = (vector)llJsonGetValue(llList2String(venue_list, i+1), ["center"]);
            vector distance = (vector)llJsonGetValue(llList2String(venue_list, i+1), ["distance"]);
            vector mypos = llGetPos();
            if (center == ZERO_VECTOR || (
                llAbs((integer)(center.x - mypos.x)) < distance.x &&
                llAbs((integer)(center.y - mypos.y)) < distance.y &&
                llAbs((integer)(center.z - mypos.z)) < distance.z
            )) {
                // Found it!
                venue_name = name;
                venue_center = center;
                venue_distance = distance;
                string venue = llLinksetDataRead("venue_" + venue_name);
                venue_version = (string)llJsonGetValue(venue, ["version"]);
#ifdef COMM_ENABLED
                integer c = (integer)llJsonGetValue(venue, ["comm", "channel"]);
                if (c != 0) {
                    COMM_CHANNEL = c;
                    tune_channel();
                }
#endif
#ifdef CAMERA_ENABLED

                cam_slots =
                    llCSV2List(llJsonGetValue(venue, ["cam_1"])) +
                    llCSV2List(llJsonGetValue(venue, ["cam_2"])) +
                    llCSV2List(llJsonGetValue(venue, ["cam_3"])) +
                    llCSV2List(llJsonGetValue(venue, ["cam_4"])) +
                    [0];
#endif
#ifndef ACT_ENABLED
#ifdef CURTAIN_ENABLED

                curtain_channel = (integer)llJsonGetValue(venue, ["curtain", "channel"]);
                curtain_close = parse_command(llJsonGetValue(venue, ["curtain", "close"]), curtain_channel);
                curtain_open = parse_command(llJsonGetValue(venue, ["curtain", "open"]), curtain_channel);
#endif
#ifdef TIPJAR_ENABLED

                tipjar_channel = (integer)llJsonGetValue(venue, ["tipjar", "channel"]);
                tipjar_login = parse_command(llJsonGetValue(venue, ["tipjar", "login"]), tipjar_channel);
                tipjar_logout += parse_command(llJsonGetValue(venue, ["tipjar", "logout"]), tipjar_channel);
#endif
#ifeq VENUE qob

                stage_channel = (integer)llJsonGetValue(venue, ["stage", "channel"]);
                stage_hide = parse_command(llJsonGetValue(venue, ["stage", "hide"]), stage_channel);
                stage_show += parse_command(llJsonGetValue(venue, ["stage", "show"]), stage_channel);
#endif
#endif
            }
        }
        i += 2;
    };

    return (venue_name != "--");
}

// ****************************************

log(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

send(integer channel, string message) {
    llRegionSay(channel, message);
    log((string)channel + ": " + message);
}

// List stride: 2 <channel>, <command>
sendlist(list commands) {
    integer i = 0;

    while (i < llGetListLength(commands)) {
        send(llList2Integer(commands, i), llList2String(commands, i+1));
        i += 2;
    }
}

string make_label() {
#ifdef ACT_ENABLED
    string retstr = venue_name;
#ifdef COMM_ENABLED
    if (COMM_CHANNEL) {
        retstr += "\\n\\n/" + (string)COMM_CHANNEL;
    }
#endif
#else
    string retstr = "| " + venue_name + " |";
#ifdef COMM_ENABLED
    if (COMM_CHANNEL) {
        retstr += " /" + (string)COMM_CHANNEL + " |";
    }
#endif
#endif
    return retstr;
}

set_text(integer show) {
    integer i = llGetNumberOfPrims();
    for (; i > 1; --i) {
        string label;
        if (show) {
            list p = llGetLinkPrimitiveParams(i, [PRIM_DESC]);
            label = SplitLine(llList2String(p, 0), "~");
        } else {
            label = "";
        }
        llSetLinkPrimitiveParamsFast(i, [
            PRIM_TEXT, label, <1,1,1>, 1
        ]);
    }
}

#ifdef COMM_ENABLED
// ****************************************
// Comm Stuff
cmd(key id, string message) {
    if (llGetSubString(message, 0, 4) == ".ping") {
        // Reply to ping
        string save_name = llGetObjectName();
        llSetObjectName(llGetUsername(owner));
        llRegionSayTo(id, -COMM_CHANNEL, "is alive");
        llSetObjectName(save_name);
    }
}
#endif

// Reset HUD
reset() {
    owner = llGetOwner();

    // Find our load script that starts with LOAD_SCRIPT_NAME
    integer count = llGetInventoryNumber(INVENTORY_SCRIPT);
    while (count--) {
        string inv_name = llGetInventoryName(INVENTORY_SCRIPT, count);
        if (llGetSubString(inv_name, 0, llStringLength(LOAD_SCRIPT_NAME)-1) == LOAD_SCRIPT_NAME) {
            load_script_name = inv_name;
        }
    }

#ifdef DISCORD_ENABLED
// ****************************************
// Discord Stuff
    haz_discord = can_haz_discord();
#endif

// ****************************************
// Venue Stuff
    // Set region-specific bits
    find_venue();
    llOwnerSay("Venue: " + venue_name + " (" + venue_version + "), " + region_name);
// ****************************************
// Settings Dialog Stuff
    // Generate a very negative menu channel
    MENU_CHANNEL = ((integer)("0x"+llGetSubString((string)llGetKey(),-8,-1)) & 0x3FFFFFFF) ^ 0xBFFFFFFF;
// ****************************************

    parse_desc(llGetObjectDesc());
    integer i = llGetNumberOfPrims();
    for (; i >= 0; --i) {
        string name;
        list p = llGetLinkPrimitiveParams(i, [PRIM_NAME, PRIM_DESC]);
        name = llList2String(p, 0);
        if (i == 1) {
            // Root link
            llSetLinkPrimitiveParamsFast(i, [
                PRIM_TEXT, SplitLine(make_label(), "~"), <1,1,1>, 1
            ]);
        }
#ifdef CAMERA_ENABLED
// ****************************************
// Camera Stuff
        if (name == "c0") {
            cam_link = i;
        }
        else if (name == "camsel") {
            camsel_link = i;
            camsel_visible = FALSE;
            do_showcam();
        }
#endif
#ifdef RLV_ENABLED
// ****************************************
// RLV Stuff
        if (name == rlv_button_name) {
            // Set the button color
            llSetLinkPrimitiveParamsFast(i, [
                PRIM_COLOR, rlv_face, UNLOCKED_COLOR, UNLOCKED_ALPHA
            ]);
            rlv_button = i;
        }
#endif
// ****************************************
    }

    set_text(TRUE);

#ifdef CAMERA_ENABLED
// ****************************************
// Camera Stuff
    // Turn off button selections
    llSetLinkPrimitiveParamsFast(camsel_link, [
        PRIM_COLOR, 0, cam_color_inactive, cam_alpha,
        PRIM_COLOR, 2, cam_color_inactive, cam_alpha,
        PRIM_COLOR, 4, cam_color_inactive, cam_alpha,
        PRIM_COLOR, 6, cam_color_inactive, cam_alpha
    ]);
#endif
#ifdef COMM_ENABLED
// ****************************************
// Comm stuff
    tune_channel();
#endif
// ****************************************

    last_attach = llGetAttached();
    attach_bottom = (llListFindList(
        [ATTACH_HUD_BOTTOM_LEFT, ATTACH_HUD_BOTTOM, ATTACH_HUD_BOTTOM_RIGHT],
        [last_attach]
    ) >= 0);

    log("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
}

default {
    state_entry() {
        llSetMemoryLimit(MEM_LIMIT);
        reset();
#ifdef RLV_ENABLED
// ****************************************
// RLV Stuff
        init_rlv();
// ****************************************
#endif
    }

    touch_start(integer num) {
        llResetTime();
    }

    touch_end(integer num) {
        integer long = (llGetTime() > 2.0);

        integer link = llDetectedLinkNumber(0);
        integer face = llDetectedTouchFace(0);
        vector pos = llDetectedTouchST(0);

        button(link, face, pos, long);
    }

    run_time_permissions(integer perm) {
        if (perm & PERMISSION_ATTACH) {
            llDetachFromAvatar( );
        }
#ifdef CAMERA_ENABLED
// ****************************************
// Camera stuff
        if (perm & PERMISSION_CONTROL_CAMERA) {
            if (cam_is_set) {
                llSetCameraParams([
                    CAMERA_ACTIVE, TRUE, // (TRUE or FALSE)
                    CAMERA_FOCUS, cam_pos+llRot2Fwd(llEuler2Rot(cam_rot * DEG_TO_RAD)), // camera rotation
                    CAMERA_FOCUS_LOCKED, TRUE, // (TRUE or FALSE)
                    CAMERA_POSITION, cam_pos, // region relative position
                    CAMERA_POSITION_LOCKED, TRUE // (TRUE or FALSE)
                ]);
                llSetLinkPrimitiveParamsFast(camsel_link, [
                    PRIM_COLOR, camsel_face, cam_color_active, cam_alpha
                ]);
            } else {
                llClearCameraParams(); // reset camera to default
                llSetLinkPrimitiveParamsFast(camsel_link, [
                    PRIM_COLOR, camsel_face, cam_color_inactive, cam_alpha
                ]);
            }
        }
        else if (perm & PERMISSION_TRACK_CAMERA) {
            llOwnerSay("\\n// camera position,rotation\\ncam_X="+(string)llGetCameraPos()+","+(string)(llRot2Euler(llGetCameraRot())*RAD_TO_DEG));
            put_cam_pos(llGetCameraPos(), llRot2Euler(llGetCameraRot())*RAD_TO_DEG, cam_index);
        }
#endif
// ****************************************
    }

// ****************************************
// HUD Positioing

    attach(key id) {
        integer attach_point = llGetAttached();
        if (id != NULL_KEY && attach_point > 0 && attach_point != last_attach) {

            // HUD rotation needs to know if we are on top or bottom
            attach_bottom = FALSE;

            // Nasty if else block
            if (attach_point == ATTACH_HUD_TOP_LEFT) {
//                log("attached: top left");
                llSetPos(<0.0, left_offset, top_offset>);
            }
            else if (attach_point == ATTACH_HUD_TOP_CENTER) {
//                log("attached: top");
                llSetPos(<0.0, center_offset, top_offset>);
            }
            else if (attach_point == ATTACH_HUD_TOP_RIGHT) {
//                log("attached: top right");
                llSetPos(<0.0, right_offset, top_offset>);
            }
//            else if (attach_point == ATTACH_HUD_CENTER_1) {
//                log("attached: center 1");
//                llSetPos(<0.0, 0.0, 0.0>);
//            }
//            else if (attach_point == ATTACH_HUD_CENTER_2) {
//                log("attached: center 2");
//                llSetPos(<0.0, 0.0, 0.0>);
//            }
            else if (attach_point == ATTACH_HUD_BOTTOM_LEFT) {
//                log("attached: bottom left");
                llSetPos(<0.0, left_offset, bot_offset>);
                attach_bottom = TRUE;
            }
            else if (attach_point == ATTACH_HUD_BOTTOM) {
//                log("attached: bottom");
                llSetPos(<0.0, center_offset, bot_offset>);
                attach_bottom = TRUE;
            }
            else if (attach_point == ATTACH_HUD_BOTTOM_RIGHT) {
//                log("attached: bottom right");
                llSetPos(<0.0, right_offset, bot_offset>);
                attach_bottom = TRUE;
            }
            last_attach = attach_point;
        }
        reset();
    }
// ****************************************

    listen(integer channel, string name, key id, string message) {
        if (channel == MENU_CHANNEL) {
            llSetTimerEvent(0);
            llListenRemove(menu_handle);
// ****************************************
// Settings Dialog stuff
            if (message == "Reload HUD") {
                reset();
            }
            else if (message == "Load Notecard") {
                llSetScriptState(load_script_name, TRUE);
                llSleep(1.0);
                llMessageLinked(LINK_SET, 42011, "", NULL_KEY);
            }
            else if (message == "List Cards") {
                list_venues();
            }
            else if (message == "Clear Data") {
                list keys = llLinksetDataFindKeys(".*", 0, -1);
                integer i = llGetListLength(keys);
                do {
                    llLinksetDataDelete(llList2String(keys, i));
                    i--;
                } while (i >= 0);
                llOwnerSay("Venue data cleared");
            }
#ifdef ACT_ENABLED
// ****************************************
// Titler stuff
            if (message == "Set Title") {
                // Display the title set dialog
                menu_handle = llListen(MENU_CHANNEL+1, "", owner, "");
                llSetTimerEvent(30.0);
                llDialog(
                    owner,
                    "ACT HUD Titler",
                    [
                        "On",
                        "Off",
                        "Set Title"
                    ],
                    MENU_CHANNEL+1
                );
            }
            else if (message == "On") {
                send(30, "|on");
            }
            else if (message == "Off") {
                send(30, "|off");
            }
        }
        else if (channel == MENU_CHANNEL+1) {
            llSetTimerEvent(0);
            llListenRemove(menu_handle);
            if (message == "Set Title") {
                // Send the new title to the titler
                llSetTimerEvent(30.0);
                menu_handle = llListen(MENU_CHANNEL+1, "", owner, "");
                llTextBox(owner, "Enter a new title", MENU_CHANNEL+1);
            }
            else {
                send(30, message);
                llOwnerSay("Setting title: \\n" + message);
            }
#endif
// ****************************************
        }
#ifdef RLV_ENABLED
// ****************************************
// RLV stuff
        else if (channel == rlv_channel) {
            llSetTimerEvent(0);
            llListenRemove(rlv_handle);

            log("RLV: " + message);
            rlv_version = (integer)message;
        }
#endif
#ifdef COMM_ENABLED
// ****************************************
// Comm stuff
        else if (channel == COMM_CHANNEL) {
            // Repeat what we say on the COMM channel
            string save_name = llGetObjectName();
            llSetObjectName(llGetUsername(id));
            llOwnerSay(comm_prefix + message);
            llRegionSay(-COMM_CHANNEL, message);
#ifdef DISCORD_ENABLED
            if (haz_discord) {
                llMessageLinked(LINK_THIS, DISCORD_SEND, message, llGetOwner());
            }
#endif
            llSetObjectName(save_name);
        }
        else if (channel == -COMM_CHANNEL) {
            // Check for received commands
            if (llGetSubString(message, 0, 0) == ".") {
                cmd(id, message);
            } else {
                // Repeat what we hear others say on the COMM channel
                string save_name = llGetObjectName();
                llSetObjectName(name);
                llOwnerSay(comm_prefix + message);

#ifdef DISCORD_ENABLED
                if (haz_discord) {
                    llMessageLinked(LINK_THIS, DISCORD_SEND, name + " " + message, llGetOwner());
                }
#endif
                llSetObjectName(save_name);
            }
        }
#endif
// ****************************************
    }

    link_message(integer sender, integer num, string str, key id) {
#ifdef DISCORD_ENABLED
#endif
        if (num == 200) {
            // Loading complete
            llSetScriptState(load_script_name, FALSE);
            reset();
        }
    }

    timer() {
        llSetTimerEvent(0);
        llListenRemove(menu_handle);
#ifdef RLV_ENABLED
// ****************************************
// RLV stuff
        llListenRemove(rlv_handle);
// ****************************************
#endif
    }

    changed(integer change) {
        if (change & (CHANGED_OWNER)) {
            llResetScript();
        }
        else if (change & (CHANGED_REGION | CHANGED_TELEPORT)) {
#ifdef CAMERA_ENABLED
            cam_is_set = FALSE;
#endif
            reset();
            llRequestPermissions(owner, PERMISSION_CONTROL_CAMERA);
        }
    }
}
