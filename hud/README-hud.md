# Stage HUD README

The Stage HUD contains controls for the various functions of some performance
venues.  Depending on the model, it may have controls for open/close the
curtains, log-in/log-out of the tipjar, and some pre-set camera positions for
common views and the backstage communicator.

Further documentation on the Stage HUD may be found at https://www.djz.one/venuekit/hud.

# Quick Reference

Here is a quick list of what buttons do.  For details skip below to the
Introduction section.

* `` [_] `` - Minimize HUD
* `` [O] `` - Open Settings popup
* `` [Lock] `` - Set RLV lock on the HUD to prevent it being removed
* `` [Camera] `` - Hide/show th camera position buttons
#ifdef CURTAIN_ENABLED
* `` [<>] [><] `` - Open and Close curtains
#endif
#ifdef TIPJAR_ENABLED
* `` [-->] [<--] `` - Log in and out of the tipjar
#endif
#ifeq VENUE qob
* `` [</>] [<o>] `` - Hide and Show thrust stage
#endif

#ifdef CAMERA_ENABLED
* `` F `` (Front) - A head-on view of the stage from the center of the house
* `` S `` (Stage) - A view of the stage from behind the main curtain
* `` B `` (Backstage) - A backstage view from roughly the center of the back wall
* `` T `` (TipJar) - A head-on view of the performer's tip jar

#endif
# Introduction

The Stage HUD has four buttons manage the HUD arranged in a square located at
the right end of the main button panel. The top pair of buttons Minimize the HUD
and open the Settings Menu, the bottom pair lock the HUD and show/hide the
camera position buttons.

The Minimize button has an underbar icon `` [_] `` and makes the HUD smaller
and restores it to full size when clicked.

The Settings button has a gear icon and opens a blue popup that contains
buttons to manage the HUD such as loading notecards and resetting scripts. A
long-press (click-and-hold the left mouse button for 2-3 seconds) on the
Settings button detaches the HUD.

The Lock button has a padlock icon that locks the HUD on your screen when it
is red. It uses RLV to handle the attachment lock so if that is not enabled
in your viewer it will not do anything.  The lock prevents the HUD from being
detached when changing costumes.  The button turns red when it is locked and
is green when unlocked.

The Camera button has a camera icon and controls a panel of camera buttons.
A complete description is found below.

## Venue Display

#ifdef COMM_ENABLED
If a venue has been detected its name and the channel used for communication
is diplayed above the HUD. If the text shown looks like this:

#ifeq VENUE 0b
    --
    /6
#else
    | -- | /6 |
#endif

then the HUD has not detected a venue.  If you think it should have detected
one reset the HUD by clicking the Settings button (gear) then select ``Reload HUD``
in the pop-up window, then it should show the venue like this:

#ifeq VENUE 0b
    ACT Main Stage
         /6
#else
    | Noir Neverland | /6 |
#endif
#else
If a venue has been detected its name is diplayed above the HUD.
If the text shown looks like this:

    | -- |

then the HUD has not detected a venue.  If you think it should have detected
one reset the HUD by clicking the Settings button (gear) then select ``Reload HUD``
in the pop-up window, then it should show the venue like this:

    | Queens of Burlesque |

#endif

#ifneq VENUE 0b
#ifdef CURTAIN_ENABLED
## Curtains

There is a set of buttons for controlling the curtains, the left `` [<>] ``
button is Open and the right `` [><] `` button is Close.

#endif
#ifdef TIPJAR_ENABLED
## Tipjar

There is a set of buttons to log in and log out of the tipjar without having to
cam over to it.  Of course there is also the Tipjar camera button (`` T ``) that
will position your camera directly in front of the tipjar.  The left `` [-->] ``
button logs in and the right `` [<--] `` button logs out of the tipjar.

#endif
#ifeq VENUE qob
## Stage

The blue buttons hide and show the thrust stage in front of the curtain. The left
`` [</>] `` button logs in and the right `` [<o>] `` button logs out of the tipjar.


#endif
#endif
#ifdef CAMERA_ENABLED
## Camera

The bottom right button with a camera icon toggles the display of the
camera position button panel so they are hidden out of the way when not needed.
It turns green when the camera buttons are visible and the HUD has
control of your viewer's camera; it turns back to white when they are hidden
and the HUD has released camera control.

The four camera position buttons act as toggles, clicking one will set your
camera to that position (press ``ESC`` once or twice if the camera does not move)
and clicking it again releases the camera position.  You can also click on a
different position button to switch there.

* `` F `` (Front) - A head-on view of the stage from the center of the house
* `` S `` (Stage) - A view of the stage from behind the main curtain
* `` B `` (Backstage) - A backstage view from roughly the center of the back wall
* `` T `` (TipJar) - A head-on view of the performer's tip jar

If you cam from any of these positions pressing ``ESC`` will return your camera to the
already selected position.  If you cam and then click the green camera button you
may need to press ``ESC`` to return your camera to your avatar.

### Changing Camera Positions

You can set new camera positions by editing the notecards that are in the object
inventory and re-loading them.  If you have camera positions already they can be
pasted into the cam_X lines (where 'X' is a number 1-4).  A camera position
consists of a position in the usual ``<x,y,z>`` vector format followed by a comma
and a rotation also in that vector format:

    cam_2=<218.50000, 200.00000, 3014.00000>, <45.32652, 14.91552, -75.72256>

The Stage HUD can record the current position of the camera and display it in
Nearby Chat in the correct format to use in a ``cam_N`` line.  Hold the left
mouse button   on the camera icon button for 2-3 seconds (aka, a long-click) and
the position will appear in Nearby Chat.

Copy and paste that into a notecard and reload the notecard via the Settings
menu.  Don't forget to change the 'X' to a number!

#endif
#ifdef COMM_ENABLED
## Communication Channel

The channel number used for performer communications is displayed in the top row
text message. Everything preceded by /6 in local/nearby chat will be broadcast to
everyone wearing this HUD set to the same channel.  This text will be displayed in
local/nearby chat and is preceded by '6>' (or whatever channel might be set) to
indicate which channel the message came from.

Only those wearing a HUD with the same channel configured will be able to see
messages sent in this manner.  Also, the messages are sent sim-wide so the
'whisper'/'shout' distances are not a factor, everyone in the same sim wearing
the same HUD will get the message.

The comm channel may be changed by setting ``comm_channel`` in the venue notecard.

#endif
## Changes

v5 2024-01-03 - Use complex curtain and tipjar commands; set venue comm channel
#ifeq VENUE qob
v5-qob 2024-12-21 - New Queens of Burlesque 4 button HUD
#endif

## Modules

#ifdef VENUE
* Venue: VENUE

#endif
#ifdef ACT_ENABLED
* ACT
#endif
#ifdef QOB_ENABLED
* QoB
#endif
#ifdef CAMERA_ENABLED
* camera
#endif
#ifdef COMM_ENABLED
* comm
#endif
#ifdef CURTAIN_ENABLED
* curtain
#endif
#ifdef DISCORD_ENABLED
* discord
#endif
#ifdef RLV_ENABLED
* rlv
#endif
#ifdef TIPJAR_ENABLED
* tipjar
#endif
