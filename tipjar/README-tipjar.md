# Tipjar README

This tipjar script is designed to be simple to use and yet flexible enough
to suit your venue. Its behaviour is controlled via a CONFIG notecard, such
as changing the public message text and setting the default pay amounts.

The script may be used in a single prim object or in a linkset.  When used
in a linkset the entire object will be clickable.  The hover text can be
displayed over one or more links in the linkset.  This is useful to have
multiple tipjars linked together so logging in/out can be done via any
of the linked objects.

# Usage

The script and CONFIG notecard can be placed in a single prim or the root of
a linkset. The hover text will be rendered above any link that has 'hover'
in its description field. This means that you could have two tip jars with
one on each side of the stage that will both show the hover text and both
work as one.  This is in fact how the example is set up.

Note that if no link has 'hover' in the description field, no hover text will
be displayed.

## Startup

When rezzed or reset the tipjar will ask the owner for Debit permissions.
This is necessary to receive and pay out money.  If the owner does not accept
the permission request the tipjar will not be usable and needs to be reset.

Once the debit permission is granted the tipjar will enter an idle state
with the message "Tipjar idle, Please click to log in"

## Logging In

Anyone who is a) the owner, or b) listed in an allow line in the CONFIG
notecard, or c) has the same group as the tipjar active may click on it to
log in.  the hover text will change to display the logged in user.  The
group requirements may be changed with the `group_login` setting in the
CONFIG notedard, see below.

The logged-in user can click on the tipjar to get a menu to display the
tippers, display the total amount received, or log out.

## Logging Out

The logged-in user can click on the tipjar to get a menu to display the
tippers, display the total amount received, or log out.  The list of tippers
is also displayed on logout so that does not need to be clicked separately.

The list of tippers is displayed in the Nearby Chat window but only for the
logged-in user.  The names of the tippers may be clicked on to open an IM
with them for a thank you if desired.

The tipjar owner or anyone listed in an `admin` line in the CONFIG notecard
(see below) are able to long-click (hold a click on the tipjar for 2 or more
seconds) and see the performer menu.  This allows an admin to log out the tipjar
in case a performer crashes or is unable to complete the logout.  The user doing
the logout will get the list of tippers in their Nearby Chat window in addition
to the logged-in performer if they are online at that time.

## HUD Use

The tipjar also listens for commands on a channel (71 by default).  The commands
it accepts are:

* `login` - This is identical to clicking on the tipjar to log in with the same
  group of alow= requirements.
* `logout` - Logs the current user out of the tipjar.  This may be used by only
  the logged-in user, the owner or anyone listed as an admin in the CONFIG
  notecard.
* `status` - Displays the current logged-in user.  Anyone may send this command.

# CONFIG notecard

The amounts in the Pay popup dialog box can be set with the `default_tip`
and `tip_buttons` lines in the CONFIG notecard.  Set `default_tip` to a single
amount and/or `tip_buttons` to up to 4 amounts to pre-populate the buttons in
the popup dialog.

Performers may log in to the tipjar if their active group matches the tipjar's
group or if they are listed in an `allow` line in the CONFIG notecard. This is
set in the CONFIG notecard by setting `group_login` to `MATCH`.  Setting
`group_login` to `ANY` causes group checking to be skipped.  Setting `group_login`
to `NONE` will only let those listed in CONFIG on `allow` lines to log in.

As noted above, the owner or anyone listed in an `admin` line in the CONFIG
notecard are able to long-click on the tipjar and see the performer menu. The
requirements for the `admin` line is the same as the `allow` line in that the
user's login or UUID must be used to identify the user.

Tips are paid to the logged-in user as they are received so in the event the user
crashes or is unable to log out no funds transfers are left outstanding. The owner
or other admin should use the long-click set described above to log out the tipjar.

The thank-you message sent to tippers can be sent in public (nearby) chat or
privately (althought it is still displayed in the tippers nearby chat window)
by setting `public_thanks` to TRUE or FALSE.

Most of the messages displayed above the tipjar may be customized via the
CONFIG notecard. There are two special tokens that can be used in the messages
to show the display name of either the logged-in performer or the tipper.

The tipjar can listen for commands from a HUD.  By default it listens on
channel 71, this can be changed by setting `channel` in the CONFIG notecard.
Setting `channel=0` turns of listening for HUD commands.

## Split Tips

The tipjar can be configured to split tips among multiple users at specific
or equal percentages.  Add as many `split` lines in the CONFIG notecard to
set the users and percentages.  If the percentage is left off, the remainder
of the tip amount after subtracting the specified percentages will be split
evenly among those without percentages.

For example, with the following settings Rebozuelo with get 10%, Pivius will
get 5% and the logged-in user will get the remaining 85%.

    split=rebozuelo,10
    split=privius,5

# Source

The tipjar script may be found online at https://gitlab.com/tomjs/venue-kit.
It is licensed as GPLv3 or later, which requires anyone who redistributes
it to make any changes they may have made available to the recipient under
the same terms they received the original source.

Layne
laynethomas@avimail.org
