// tipjar.lsl - Basic tipjar script
// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2020 Layne Thomas

// v1 28Mar2020 laynethomas@avimail.org
//   - change notecard comment char to '#'
//   - remove tipper menu
// v2 27Jun2020 laynethomas@avimail.org
//   - Set hover text on a link with 'hover' in the description if one exists
// v3 05Aug2020 laynethomas@avimail.org
//   - Use CONFIG notecard
//   - Listen for commands from HUD
//   - Remove roaming and spoils code
//   - Allow group membership for login
// v4 06Aug2020 laynethomas@avimail.org
//   - Remove debit confirmation dialog (the llDialog() one)
//   - Tighten up the login/logout mesages to clarify what is happening
// v5 08Aug2020 laynethomas@avimail.org
//   - Replace llInstantMessage() with llRegionSayTo() - SPEED!
// v6 09Aug2021 laynethomas@avimail.org
//   - Add split handling
// v7 20Dec2023 laynethomas@avimail.org
//   - Add group_login
//   - Expand status display

// Portions derived from http://wiki.secondlife.com/wiki/Tipjar
// [K] Kira Komarov - 2011, License: GPLv3

// Config notecard

// public_thanks=true
// default_price=51
// price_buttons=23, 57, 79, 131
// channel=<num>
// group_login=match|any|none
// admin=<name>|<uuid>
// allow=<name>|<uuid>
// idle=<msg>
// loading=<msg>
// overhead=<msg>
// thanks=<msg>
// touch=<msg>

string IDLE_MESSAGE = "Tipjar idle, please click to log in";
string LOADING_MESSAGE = "Loading configuration\nPlease wait\n";
string LOGIN_FAIL_MESSAGE = "Sorry, you are not authorized to use this tip jar.  Check your groups if you think it should work.";
string OVERHEAD_MESSAGE = "%login%'s Tipjar\nAny tip is welcome!";
string THANKS_MESSAGE = "Thank you for your tip %tipper%! %login% greatly appreciates it!";
string TOUCH_MESSAGE = "Hello %tipper%! Please consider giving %login% a tip by right-clicking the tipjar and selecting Pay then entering an amount. Thank you!";

string CONFIG_NOTECARD = "CONFIG";
integer PUBLIC_THANKS = FALSE;
integer DEFAULT_PRICE = 51;
list PRICE_BUTTONS = [23, 57, 79, 131];
integer DEFAULT_HUD_CHANNEL = 71;

list tippers;
list tip_amounts;
integer tip_total;

list conf_split_list;
list conf_percent_list;
list real_percent_list;
float total_percent;
integer num_no_percent;
float login_percent;

// 0 - match
// 1 - any
// 2 - none
list group_login_str = ["match", "any", "none"];
integer group_login = 0;

string login_name;
key login_key;
string login_display_name;

list access_list;
list admin_list;
key owner_key;

list hover_links = [];
integer hud_channel;
integer hud_handle;
integer menu_channel;
integer menu_handle;
list menu_dialog = ["[ Tippers ]", "[ Total ]", "[ Logout ]"];
integer logging_out;

// Taken from http://wiki.secondlife.com/wiki/Tipjar
// %login% is replaced with logged in avatar's display name
// %tipper% is replaced with tipping avatar's display name
string tokenSubstitute(string input, key id) {
    list kSubst = llParseString2List(input, ["%"], [""]);
    integer itra;
    for(itra = 0; itra < llGetListLength(kSubst); ++itra) {
        if(llList2String(kSubst, itra) == "login")
            kSubst = llListReplaceList(kSubst, (list)login_display_name, itra, itra);
        if(llList2String(kSubst, itra) == "tipper")
            kSubst = llListReplaceList(kSubst, (list)llGetDisplayName(id), itra, itra);
    }
    return llDumpList2String(kSubst, "");
}

// Taken from http://wiki.secondlife.com/wiki/NewLine
string newline(string message) {
    list lWords = llParseStringKeepNulls(message, ["\\n"], []);
    return llDumpList2String(lWords, "\n");
}

// authorized(ID)
// Return TRUE if:
// * owner
// * group matches object group (depending on group_login setting)
// * key in access_list
// * name(key) in access_list
integer authorized(key id) {
    if (id == NULL_KEY || id == "") {
        // If no key given, fail auth
        return FALSE;
    }

    string username = llGetUsername(id);
    if (username == "") {
        // id is a prim
        id = llGetOwnerKey(id);
        username = llGetUsername(id);
    }
    if (group_login == 1) {
        // any - anyone may log in
        return TRUE;
    }
    integer same_group = llSameGroup(id);
    if (group_login == 2) {
        // none - group check is always FALSE forcing the check to
        // access_list or owner only
        same_group = FALSE;
    }
    return (
        id == llGetOwnerKey(llGetKey()) ||
        same_group ||
        llListFindList(access_list, [(string)id]) >= 0 ||
        llListFindList(access_list, [username]) >= 0
    );
}

// admin(ID)
// Return TRUE if:
// * owner
// * key in admin_list
// * name(key) in admin_list
integer admin(key id) {
    string username = llGetUsername(id);
    return (
        id == llGetOwnerKey(llGetKey()) ||
        llListFindList(admin_list, [(string)id]) >= 0 ||
        llListFindList(admin_list, [username]) >= 0
    );
}

set_hover_text(string message) {
    integer i = llGetListLength(hover_links);
    while (i--) {
        llSetLinkPrimitiveParamsFast(llList2Integer(hover_links, i), [
            PRIM_TEXT, message, <1.0,1.0,1.0>, 1.0
        ]);
    }
}

// Calculate tip split percentages
calc_split(integer add_login) {
    // split=<name>|<uuid>[,<percent>]

    // Finalize split percentages
    integer num_splits = llGetListLength(conf_split_list);
    if (num_splits > 0) {
        float remaining_percent = 100 - total_percent;
        // check for overflow here!
        if (remaining_percent < 0.0) {
            llOwnerSay("Split config error, percentage is > 100%: " + (string)remaining_percent);
        }
        llOwnerSay("Configured tip splits:");
        integer i;
        for (i=0; i < llGetListLength(conf_split_list); i++) {
            integer login_has_split = FALSE;

            // Get split config
            string id = llList2String(conf_split_list, i);
            float share = (float)llList2String(conf_percent_list, i);

            // Check for a split with no percentage
            if (share <= 0.0) {
                // Save the amount of the split
                // Yeah, using add_login here is janky, we're assuming that
                // caller is passing in 0/FALSE or 1/TRUE
                share = remaining_percent / (num_no_percent + add_login);
            }
            real_percent_list += share;
            llOwnerSay("  " + llList2String(conf_split_list, i) + ": "+(string)share + "%");
        }
        if (add_login) {
            login_percent = remaining_percent / (num_no_percent + add_login);
            llOwnerSay("  " + login_name + ": " + (string)login_percent + "%");
        }
    } else {
        llOwnerSay("No tip splits configured");
    }
}

// Read CONFIG notecard

key notecard_qid;
integer line;
integer reading_notecard;
string lineodots;

read_config(string data) {
    if (data == EOF) {
        // All done
        return;
    }

    data = llStringTrim(data, STRING_TRIM_HEAD);
    lineodots += ".";
    set_hover_text(LOADING_MESSAGE + lineodots);
    if (data != "" && llSubStringIndex(data, "#") != 0) {
        integer i = llSubStringIndex(data, "=");
        if (i != -1) {
            lineodots += ".";
            set_hover_text(LOADING_MESSAGE + lineodots);
            string attr = llToLower(llStringTrim(llGetSubString(data, 0, i-1), STRING_TRIM));
            string value = llStringTrim(llGetSubString(data, i+1, -1), STRING_TRIM);

            if (attr == "admin") {
                // admin=<name>|<uuid>
                // admin avatars
                admin_list += value;
            }
            else if (attr == "allow") {
                // allow=<name>|<uuid>
                // Explicitly allowed avatars
                access_list += value;
            }
            else if (attr == "group_login") {
                // group_login=match|any|none
                group_login = llListFindList(group_login_str, [llToLower(value)]);
                if (group_login < 0) {
                    llOwnerSay("Unknown value for group_login: " + value + ", using 'MATCH'");
                    group_login = 0;
                }
            }
            else if (attr == "split") {
                // split=<name>|<uuid>[,<percent>]
                // Set up tip splits
                list v = llParseString2List(value, [",", " "], []);
                conf_split_list += llList2String(v, 0);
                float share = (float)llList2String(v, 1);
                if (share > 0.0) {
                    total_percent += share;
                } else {
                    num_no_percent++;
                }
                conf_percent_list += share;
            }
            else if (attr == "public_thanks") {
                PUBLIC_THANKS = llListFindList(["f", "false", "0"], [llToLower(value)]) < 0;
            }
            else if (attr == "default_price" || attr == "default_tip") {
                DEFAULT_PRICE = (integer)value;
            }
            else if (attr == "price_buttons" || attr == "tip_buttons") {
                list v = llParseString2List(value, [",", " "], []);
                integer j;
                PRICE_BUTTONS = [];
                for (j = 0; j < llGetListLength(v); j++) {
                    PRICE_BUTTONS += (integer)llList2String(v, j);
                }
            }
            else if (attr == "channel") {
                hud_channel = (integer)value;
            }
            else if (attr == "idle") {
                // Save attr somewhere for later
                IDLE_MESSAGE = newline(value);
            }
            else if (attr == "overhead") {
                // Save attr somewhere for later
                OVERHEAD_MESSAGE = newline(value);
            }
            else if (attr == "thanks") {
                // Save attr somewhere for later
                THANKS_MESSAGE = newline(value);
            }
            else if (attr == "touch") {
                // Save attr somewhere for later
                TOUCH_MESSAGE = newline(value);
            }
        //     else {
        //         // llOwnerSay("Unknown configuration value: " + name + " on line " + (string)line);
        //     }
        // } else {
        //     // not an assignment line
        //     // llOwnerSay("Configuration could not be read on line " + (string)line);
        }
    }
    notecard_qid = llGetNotecardLine(CONFIG_NOTECARD, ++line);
}

integer login(key id) {
    if (authorized(id)) {
        login_key = id;
        login_name = llDetectedName(0);
        login_display_name = llGetDisplayName(login_key);
        return TRUE;
    }
    return FALSE;
}

logout(key id) {
    set_hover_text("Tipjar logging out " + login_display_name + "\nPlease wait...");
    llRegionSayTo(id, 0, "Tipjar logging out " + login_display_name + ", please wait...");
    list_tippers(id);
    if (id != login_key) {
        llRegionSayTo(login_key, 0, "Tipjar logging out " + login_display_name + ", please wait...");
        list_tippers(login_key);
        llRegionSayTo(login_key, 0, "Tipjar idle");
    }
    llRegionSayTo(id, 0, "Tipjar idle");
    login_key = NULL_KEY;
    login_name = "";
    login_display_name = "";
}

list_tippers(key id) {
    integer i;
    llRegionSayTo(id, 0, "===== You received tips from:");
    for (i = 0; i < llGetListLength(tippers); ++i) {
        llRegionSayTo(id, 0, "secondlife:///app/agent/"+ (string)llList2Key(tippers, i) + "/im has tipped you: L$" + llList2String(tip_amounts, i));
    }
    llRegionSayTo(id, 0, "===== Total: L$" + (string)tip_total);
}

string tipjar_status() {
    string ret = "Tipjar status:\n";
    if (login_key == NULL_KEY) {
        // No login
        ret += "Tipjar is idle\n";
    } else {
        ret += "secondlife:///app/agent/"+ (string)login_key + "/im is logged in\n";
    }
    if (conf_split_list != []) {
        ret += "Tips are being split\n";
    }
    ret += "group_login is set to " + llList2String(group_login_str, group_login) + "\n";
    return ret;
}

default {
    state_entry() {
        owner_key = llGetOwner();
        reading_notecard = FALSE;
        hud_channel = DEFAULT_HUD_CHANNEL;
        menu_channel = ((integer)("0x" + llGetSubString((string)llGetOwner(), -8, -1)) & 0x3FFFFFFF) ^ 0xBFFFFFFF;

        integer i;
        integer num_links;
        num_links = llGetNumberOfPrims();
        for (; i <= num_links; ++i) {
            list p = llGetLinkPrimitiveParams(i, [PRIM_DESC]);
            string desc = llList2String(p, 0);
            if (desc == "hover") {
                hover_links += i;
            }
        }

        // Load config notecard
        for (i = 0; i < llGetInventoryNumber(INVENTORY_NOTECARD); ++i) {
            if (llGetInventoryName(INVENTORY_NOTECARD, i) == CONFIG_NOTECARD) {
                set_hover_text(lineodots);
                reading_notecard = TRUE;
                access_list = [];
                conf_split_list = [];
                conf_percent_list = [];
                real_percent_list = [];
                total_percent = 0.0;
                num_no_percent = 0;
                line = 0;
                lineodots = "";
                notecard_qid = llGetNotecardLine(CONFIG_NOTECARD, line);
                return;
            }
        }
        set_hover_text(CONFIG_NOTECARD + " notecard not found");
        llRegionSayTo(owner_key, 0, CONFIG_NOTECARD + " notecard not found");
    }

    changed(integer change) {
        if (change & (CHANGED_OWNER | CHANGED_INVENTORY))
            llResetScript();
    }

    dataserver(key id, string data) {
        if (id == notecard_qid) {
            read_config(data);
            if (data == EOF) {
                // Finalize split percentages
                calc_split(FALSE);
                if (!(llGetPermissions() & PERMISSION_DEBIT)) {
                    // Get permission up-front so owner doesn't have to wait for
                    // someone to log in to grant permission
                    set_hover_text("Owner must confirm permission...");
                    llRequestPermissions(llGetOwner(), PERMISSION_DEBIT);
                }
            }
        }
    }

    run_time_permissions(integer perm) {
        if (perm & PERMISSION_DEBIT) {
            llOwnerSay("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
            llOwnerSay("Listening on channel " + (string)hud_channel);
            llOwnerSay("Tipjar ready");
            state idle;
        }
    }

    touch_start(integer num) {
        key id = llDetectedKey(0);
        if (reading_notecard) {
            llRegionSayTo(id, 0, "Please wait while configuration is loaded");
            llResetScript();
        }
        // Only reach here when debit has not been confirmed
        set_hover_text("Please click to reset tipjar");
    }
}

state idle {
    state_entry() {
        login_key = NULL_KEY;
        login_name = "";
        login_percent = 0.0;
        tip_total = 0;
        tippers = [];
        tip_amounts = [];
        logging_out = FALSE;

        if (hud_channel != 0) {
            hud_handle = llListen(hud_channel, "", "", "");
        }
        set_hover_text(IDLE_MESSAGE);

        if (num_no_percent > 0) {
            // Handle the no-login mode when there is a split with no percentages
            login_key = owner_key;
            calc_split(FALSE);
            state tipjar;
        }
    }

    changed(integer change) {
        if (change & (CHANGED_OWNER | CHANGED_INVENTORY))
            llResetScript();
    }

    touch_start(integer num) {
        llResetTime();
    }

    touch_end(integer num) {
        key id = llDetectedKey(0);
        if (llGetTime() > 1.5 && admin(id)) {
            // Admin must long-click for menu
            menu_handle = llListen(menu_channel, "", "", "");
            llDialog(id, "Tipjar: Please choose an option:\n", menu_dialog, menu_channel);
            return;
        }

        if (login(id)) {
            set_hover_text("Tipjar logging in " + login_display_name + ", please wait...");
            calc_split(TRUE);
            state tipjar;
        } else {
            // Unauthorized
            llRegionSayTo(id, 0, LOGIN_FAIL_MESSAGE);
        }
    }

    listen(integer channel, string name, key id, string message) {
        // Check key to listen for a HUD
        if (llGetUsername(id) == "") {
            // id is a prim, get the owner
            id = llGetOwnerKey(id);
        }

        if (authorized(id)) {
            if (llToLower(message) == "login") {
                if (login(id)) {
                    calc_split(TRUE);
                    state tipjar;
                }
            }
            else if (llToLower(message) == "status") {
                llRegionSayTo(id, 0, tipjar_status());
            }
        } else {
            // Unauthorized
            llRegionSayTo(id, 0, LOGIN_FAIL_MESSAGE);
        }
    }
}

state tipjar {
    state_entry() {
        llRegionSayTo(login_key, 0, "Tipjar logged in " + login_display_name + ", good luck!");
        llSetPayPrice(DEFAULT_PRICE, PRICE_BUTTONS);
        set_hover_text(tokenSubstitute(OVERHEAD_MESSAGE, login_key));
        if (hud_channel != 0) {
            hud_handle = llListen(hud_channel, "", "", "");
        }
    }

    touch_start(integer num) {
        llResetTime();
    }

    touch_end(integer num) {
        key id = llDetectedKey(0);

        if (id == login_key || (llGetTime() > 1.5 && admin(id))) {
            // Logged-in user or owner can see this menu, admin must long-click
            menu_handle = llListen(menu_channel, "", "", "");
            llDialog(id, "Tipjar: Please choose an option:\n", menu_dialog, menu_channel);
            return;
        }
        llRegionSayTo(id, 0, tokenSubstitute(TOUCH_MESSAGE, id));
    }

    listen(integer channel, string name, key id, string message) {
        // Check key to listen for a HUD
        if (llGetUsername(id) == "") {
            // id is a prim, get the owner
            id = llGetOwnerKey(id);
        }

        if (llToLower(message) == "status") {
            // Anyone can check status
            llRegionSayTo(id, 0, tipjar_status());
        }
        else if (id == login_key || admin(id)) {
            if (channel == menu_channel)
                llListenRemove(menu_handle);
            if (llToLower(message) == "menu") {
                // Logged-in user or owner can see this menu, admin must long-click
                menu_handle = llListen(menu_channel, "", "", "");
                llDialog(id, "Tipjar: Please choose an option:\n", menu_dialog, menu_channel);
            }
            else if (message == "[ Logout ]" || llToLower(message) == "logout") {
                if (logging_out) {
                    // Logout already in progress
                    llRegionSayTo(id, 0, "Tipjar already logging out " +login_display_name + ", please wait...");
                } else {
                    logging_out = TRUE;
                    logout(id);
                    state idle;
                }
            }
            else if (message == "[ Tippers ]") {
                list_tippers(id);
            }
            else if (message == "[ Total ]") {
                llRegionSayTo(id, 0, login_display_name + " total tips: L$" + (string)tip_total);
            }
        }
    }

    money(key id, integer amount) {
        if (~llListFindList(tippers, (list)id)) {
            integer tip = amount + llList2Integer(tip_amounts, llListFindList(tippers, (list)id));
            tip_amounts = llListReplaceList(tip_amounts, (list)tip, llListFindList(tippers, (list)id), llListFindList(tippers, (list)id));
        } else {
            tippers += id;
            tip_amounts += amount;
        }
        tip_total += amount;
        if (PUBLIC_THANKS)
            llShout(PUBLIC_CHANNEL, tokenSubstitute(THANKS_MESSAGE, id));
        else
            llRegionSayTo(id, 0, tokenSubstitute(THANKS_MESSAGE, id));

        if (llGetListLength(conf_split_list) > 0) {
            integer i;
            for (i=0; i < llGetListLength(conf_split_list); i++) {
                // Get split
                string split_id = llList2String(conf_split_list, i);
                float share = (float)llList2String(real_percent_list, i);
                llRegionSayTo(split_id, 0, llGetDisplayName(id) + " has just tipped you: L$" + (string)amount + ".");
                llGiveMoney(split_id, (integer)(amount * share / 100));
            }
        } else {
            llRegionSayTo(login_key, 0, llGetDisplayName(id) + " has just tipped you: L$" + (string)amount + ".");
            llGiveMoney(login_key, amount);
        }
    }
}
