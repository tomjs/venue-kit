# Pre-processing Makefile for LSL

BUILD_DIR := build

all: build

build:
	mkdir -p $(BUILD_DIR)

GPP_OPTS=+c "//\#" "\\n" -U "" "" "(" "," ")" "(" ")" "\#" "\\" -M "\\n\#\\w" "\\n" "\\b" "\\b" "\\n" "(" ")"

# ARGS - optional macro defines and whatnot
# VENUE - really not optional venue qualifier
venue-hud:
	gpp $(GPP_OPTS) $(ARGS) -DVENUE=$(VENUE) hud/venue_hud.lsl -O $(BUILD_DIR)/venue_hud-$(VENUE).lsl
	gpp $(GPP_OPTS) $(ARGS) -DVENUE=$(VENUE) -DVENUE_ENABLED hud/load_notecard.lsl -O $(BUILD_DIR)/load_venue.lsl
	gpp $(GPP_OPTS) $(ARGS) -DVENUE=$(VENUE) hud/discord.lsl -O $(BUILD_DIR)/discord-$(VENUE).lsl
	gpp $(GPP_OPTS) $(ARGS) -DVENUE=$(VENUE) hud/README-hud.md -O $(BUILD_DIR)/README-$(VENUE).md

vh-all: build
	$(MAKE) venue-hud ARGS="-DCAMERA_ENABLED -DCOMM_ENABLED -DCURTAIN_ENABLED -DRLV_ENABLED -DTIPJAR_ENABLED" VENUE=all

# HUDs

vh-cam: build
	$(MAKE) venue-hud ARGS="-DCAMERA_ENABLED" VENUE=cam

vh-discord: build
	$(MAKE) venue-hud ARGS="-DCOMM_ENABLED -DDEFAULT_COMM_CHANNEL=9 -DCURTAIN_ENABLED -DDISCORD_ENABLED -DRLV_ENABLED -DTIPJAR_ENABLED" VENUE=discord

vh-display: build
	$(MAKE) venue-hud ARGS="-DCURTAIN_ENABLED -DRLV_ENABLED -DTIPJAR_ENABLED" VENUE=display

# Venues

vh-0b: build
	$(MAKE) venue-hud ARGS="-DCAMERA_ENABLED -DCOMM_ENABLED -DRLV_ENABLED" VENUE=0b

vh-2b: build
	$(MAKE) venue-hud ARGS="-DCAMERA_ENABLED -DCOMM_ENABLED -DCURTAIN_ENABLED -DRLV_ENABLED" VENUE=2b

vh-4b: build
	$(MAKE) venue-hud ARGS="-DCAMERA_ENABLED -DCOMM_ENABLED -DCURTAIN_ENABLED -DRLV_ENABLED -DTIPJAR_ENABLED" VENUE=4b

vh-act:
	$(MAKE) venue-hud ARGS="-DACT_ENABLED -DCAMERA_ENABLED -DCOMM_ENABLED -DDEFAULT_COMM_CHANNEL=8 -DRLV_ENABLED" VENUE=act

vh-actcam:
	$(MAKE) venue-hud ARGS="-DACT_ENABLED -DCAMERA_ENABLED -DRLV_ENABLED" VENUE=actcam

vh-aud: vh-audace

vh-audace:
	$(MAKE) venue-hud ARGS="-DCAMERA_ENABLED -DCURTAIN_ENABLED -DRLV_ENABLED -DTIPJAR_ENABLED -DDEFAULT_TIPJAR_CHANNEL=77" VENUE=audace

# use 4b
vh-jd: vh-4b
#	$(MAKE) venue-hud ARGS="-DCURTAIN_ENABLED -DRLV_ENABLED" VENUE=jd

# use 4b
vh-noir: vh-4b
#	$(MAKE) venue-hud ARGS="-DCAMERA_ENABLED -DCOMM_ENABLED -DDEFAULT_COMM_CHANNEL=6 -DCURTAIN_ENABLED -DRLV_ENABLED -DTIPJAR_ENABLED" VENUE=noir

vh-qob: build
	$(MAKE) venue-hud ARGS="-DQOB_ENABLED -DCAMERA_ENABLED -DCURTAIN_ENABLED -DRLV_ENABLED" VENUE=qob

vh-ss:
	$(MAKE) venue-hud ARGS="-DCAMERA_ENABLED -DRLV_ENABLED" VENUE=ss

# use 4b
vh-state: vh-4b
#	$(MAKE) venue-hud ARGS="-DCAMERA_ENABLED -DCOMM_ENABLED -DDEFAULT_COMM_CHANNEL=6 -DCURTAIN_ENABLED -DRLV_ENABLED -DTIPJAR_ENABLED" VENUE=state

# Scripts

build-hide:
	gpp $(GPP_OPTS) $(ARGS) scripts/hide_show_color.lsl -O $(BUILD_DIR)/hide_show$(HIDE_NAME).lsl

hide-all: hide hide-color

hide:
	$(MAKE) build-hide ARGS="" HIDE_NAME=""

hide-color:
	$(MAKE) build-hide ARGS="-DCOLOR_ENABLED" HIDE_NAME="_color"


clean:
	rm -f $(BUILD_DIR)/*
